<?php
/**
 * @author <Akartis>
 * (c) akartis-dev <sitrakaleon23@gmail.com>
 * Do it with love
 */

namespace App\Entity\Form;

/**
 * Class MailForm
 * @package App\Entity\Form
 */
class MailForm
{
	/**
	 * @var string | null
	 */
	private ?string $email;

	/**
	 * @return string|null
	 */
	public function getEmail(): ?string
	{
		return $this->email;
	}

	/**
	 * @param string|null $email
	 * @return MailForm
	 */
	public function setEmail(?string $email): MailForm
	{
		$this->email = $email;
		return $this;
	}
}
