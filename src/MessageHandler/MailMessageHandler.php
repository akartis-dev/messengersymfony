<?php
/**
 * @author <Akartis>
 * (c) akartis-dev <sitrakaleon23@gmail.com>
 * Do it with love
 */

namespace App\MessageHandler;


use App\Message\MailMessage;
use App\Service\MailNotification;
use Symfony\Component\Messenger\Handler\MessageHandlerInterface;

class MailMessageHandler implements MessageHandlerInterface
{
	private MailNotification $notification;

	public function __construct(MailNotification $notification)
	{
		$this->notification = $notification;
	}

	public function __invoke(MailMessage $mail)
	{
		$this->notification->sendMail($mail->getContent());
	}
}
